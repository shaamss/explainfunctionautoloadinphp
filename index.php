<?php 
/*
* ToDo (qualified full path)
*/

// get path of exists file
define('APP_PATH', dirname(realpath(__FILE__)));

//echo APP_PATH;
//echo '<br/>';
//echo DIRECTORY_SEPARATOR;

// get dir separator Depending on OS windows or unix or linux ;
define('DS', DIRECTORY_SEPARATOR);

// get path separator Depending on OS windows or unix or linux ;
define('PS',PATH_SEPARATOR);
//echo DS;
//echo '<br/>';

// get full path for my directory 
define('CONTROLLERS_PATH',APP_PATH . DS . 'controllers');
// get full path for my directory 
define('MODELS_PATH',APP_PATH . DS . 'models');
//get ALL paths for ALL Directories AND directory_separator AND path_separator ;
$paths =  get_include_path() . PS . CONTROLLERS_PATH . PS . MODELS_PATH . PS;
//echo $paths;

//set my paths in all php paths;
set_include_path($paths);

// function my __autolaod();
function myAutoLoader($className)
{
    require strtolower($className) . '.class.php';
}
// i registered myFunctionAutoLoad in standard php library;
spl_autoload_register('myAutoLoader');

$a = new A;
$b = new b;
$c = new c;